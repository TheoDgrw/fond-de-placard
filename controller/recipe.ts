import express = require('express');
const recipeRepo = require('../repository/RecipeRepo');

class RecipeController {

    public async getGetRecipe(req: express.Request, res: express.Response, next: express.NextFunction) {
        const result = await recipeRepo.getRecipeByName(req.params.name);
        console.log(result);

        res.json(result);
    }

    public async postAddRecipe(req: express.Request, res: express.Response, next: express.NextFunction) {
        // name: string, category: string, picture: string, ingredients: string[]
        const recipe = req.body.recipe;
        const result = await recipeRepo.addRecipe(recipe.name, recipe.category, recipe.picture, recipe.ingredients);

        res.json(result);
    }

    public async getUpVote(req: express.Request, res: express.Response, next: express.NextFunction) {
        const result = await recipeRepo.upVote(req.params.name);

        return result;
    }

    public async getDownVote(req: express.Request, res: express.Response, next: express.NextFunction) {
        const result = await recipeRepo.downVote(req.params.name);

        return result;
    }

    public async postCorrectRecipe(req: express.Request, res: express.Response, next: express.NextFunction) {
        const recipe = req.body.recipe;
        const ingredients = req.body.ingredients;
        const result = await recipeRepo.correctRecipe(
            recipe.name,
            recipe.new_category,
            recipe.new_picture,
            ingredients.to_add,
            ingredients.to_delete
        );

        return result;
    }

}

module.exports = new RecipeController();

import express = require('express');

const ingredientRepo = require('../repository/IngredientRepo');
const pool = require('../utils/database').pool;

class IngredientController {

    public async postAddIngredient(req: express.Request, res: express.Response, next: express.NextFunction) {
        console.log('coucou');
        const ingredient: string = req.body.ingredient;
        const result = await ingredientRepo.addIngredient(ingredient);
        res.json(result);
    }
}

module.exports = new IngredientController();

import mysql = require('mysql2/promise');

class Connection {
    public pool: mysql.Pool;

    constructor() {
        this.pool = mysql.createPool({
            host: 'localhost',
            user: 'root',
            password: 'root',
            database: 'fond_de_placard',
            waitForConnections: true,
            connectionLimit: 10,
            queueLimit: 0,
            socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
        });
        this.createRecipeTable();
        this.createIngredientTable();
        this.createRecipeIngredientTable();
    }

    private createRecipeTable(): void {
        this.pool.query(`
            CREATE TABLE IF NOT EXISTS recipe (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
                parent_id INT UNSIGNED,
                name VARCHAR(255) NOT NULL,
                category VARCHAR(255) NOT NULL,
                picture TEXT NOT NULL,
                score INT NOT NULL DEFAULT 0,
                validation INT(2) NOT NULL DEFAULT 0,
                CONSTRAINT FOREIGN KEY (parent_id) REFERENCES recipe(id)
            )
        `);
    }

    private createRecipeIngredientTable(): void {
        this.pool.query(`
            CREATE TABLE IF NOT EXISTS recipe_ingredient (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
                recipe_id INT UNSIGNED NOT NULL,
                ingredient_id INT UNSIGNED NOT NULL,
                CONSTRAINT FOREIGN KEY (recipe_id) REFERENCES recipe(id),
                CONSTRAINT FOREIGN KEY (ingredient_id) REFERENCES ingredient(id)
            )
        `);
    }

    private createIngredientTable(): void {
        this.pool.query(`
            CREATE TABLE IF NOT EXISTS ingredient (
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
                name VARCHAR(255) NOT NULL
            )
        `);
    }
}

module.exports = new Connection();

import express = require('express');

const ingredientRoutes = require('./ingredient');
const recipeRoutes = require('./recipe');

const router = express.Router();

router.use('/ingredient', ingredientRoutes);
router.use('/recipe', recipeRoutes);

module.exports = router;

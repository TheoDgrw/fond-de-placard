import express = require('express');

const recipeController = require('../controller/recipe');

const router = express.Router();

router.post('/add-recipe', recipeController.postAddRecipe);
router.post('/correct', recipeController.postCorrectRecipe);
router.get('/up-vote', recipeController.getUpVote);
router.get('/down-vote', recipeController.getDownVote);
router.get('/get/:name', recipeController.getGetRecipe);

module.exports = router;

import express = require('express');

const ingredientController = require('../controller/ingredient');

const router = express.Router();

router.post('/add-ingredient', ingredientController.postAddIngredient);

module.exports = router;

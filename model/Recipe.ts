import { Ingredient } from './Ingredient';

interface RecipeType {
    id: number;
    name: string;
    category: string;
    score: number;
    picture: string;
    validation: number;
}

// export function createEntities(query: any[]) {
//     const entities: Ingredient[] = [];
//     query[0][0].forEach((recipe: RecipeType) => {
//         entities.push(new Recipe(
//             recipe.id,
//             recipe.name,
//             recipe.category,
//             recipe.picture,
//             recipe.score,
//             recipe.validation,
//             recipe.
//         ));
//     });
//
//     return entities;
// }

export class Recipe {
    public id: number;

    public name: string;

    public category: string;

    public score: number;

    public picture: string;

    public validation: number;

    public ingredients: Ingredient[];

    constructor(id: number, name: string, category: string, picture: string, score: number, validation: number, ingredients: Ingredient[]) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.score = score;
        this.picture = picture;
        this.validation = validation;
        this.ingredients = ingredients;
    }
}

const pool = require('../utils/database').pool;
import { Recipe } from './Recipe';

interface IngredientInterface {
    id: number;
    name: string;
}

export function createEntities(query: any[]) {
    const entities: Ingredient[] = [];
    query[0][0].forEach((ingredient: IngredientInterface) => {
        entities.push(new Ingredient(ingredient.id, ingredient.name))
    });

    return entities;
}

export class Ingredient {
    public id: number;

    public name: string;

    // public recipes: Recipe[];

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;

    }
}

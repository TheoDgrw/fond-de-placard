import { Pool, Query } from 'mysql2/promise';
import { IngredientInterface } from "../interface/IngredientInterface";

const pool = require('../utils/database').pool;

class RecipeRepo {
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    public async getRecipeByName(name: string)  {
        const res = await this.pool.query(`
            SELECT * from recipe r, ingredient i
            INNER JOIN recipe_ingredient r_i ON r_i.recipe_id = r.id
            INNER JOIN ingredient i ON i.id = r_i.ingredient_id
            WHERE r.name LIKE "${name}"
            GROUP BY r.id
        `);

        return res;
    }

    public async getRecipesByIngredients(ingredientsNames: string[]) {
        const res = await this.pool.query(`
            SELECT * from recipe r
            INNER JOIN recipe_ingredient r_i ON r_i.recipe_id = r.id
            INNER JOIN ingredient i ON i.id = r_i.ingredient_id
            GROUP BY r.id
        `);

        console.log(res);

        // TODO: filter to only get recipes with all the ingredients

        return res;
    }

    public async addRecipe(name: string, category: string, picture: string, ingredients: string[]) {
        const res = await this.pool.query(`
            INSERT INTO recipe SET name = "${name}", category = "${category}", picture = "${picture}"
        `);
        ingredients.forEach(async ingredient => {
            const [rows, fields] = await this.pool.query(`
                SELECT * from ingredient WHERE name = "${ingredient}"
            `);
            if (Array.isArray(rows) && rows.length) {
                console.log(rows);
                for (let row of rows) {
                    console.log((<IngredientInterface>row).id);
                }
                // rows.forEach(row: any => {
                //         this.pool.query(`
                //             INSERT INTO recipe_ingredient SET ingredient_id = "${row.id}", recipe_id = "${}"
                //         `);
                // });
            }
        });

        return res;
    }

    public async correctRecipe(
        name: string,
        newCategory: string | null,
        picture: string | null,
        newIngredients: string[],
        ingredientsToDelete: string[]
    ) {
        const recipe = await this.getRecipeByName(name);
        console.log(recipe);
        // TODO: créer une nouvelle recette avec comme parent_id l'id de la recette à corriger
    }

    public validateRecipe(name: string): void {
        // TODO: valider une recette en cherchant celle qui a comme parent_id la recette du nom donné
    }

    public upVote(name: string) {
        this.pool.query(`
            UPDATE recipe
            SET score = score + 1
            WHERE name = ${name}
        `);
    }

    public downVote(name: string) {
        this.pool.query(`
            UPDATE recipe
            SET score = score + 1
            WHERE name = ${name}
        `);
    }
}

module.exports = new RecipeRepo(pool);

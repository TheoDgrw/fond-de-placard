import { Pool, Query } from 'mysql2';

const pool = require('../utils/database').pool;

class IngredientRepo {
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    public async addIngredient(name: string, category: string, picture: string, score: number) {
        const res = await this.pool.query(`
            INSERT INTO ingredient SET name = ?
        `, [name]);

        return res;
    }

    public async getIngredients(name: string): Promise<Query> {
        const res = await this.pool.query(`
            SELECT name from ingredient i
            WHERE i.name LIKE "%${name}%"
        `);
        return res;
    }
}

module.exports = new IngredientRepo(pool);

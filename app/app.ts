import express = require('express');
const bodyParser = require('body-parser');

const con = require('../utils/database');
const routes = require('../routes');

const app = express.application = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(routes);

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.listen(3000, () => {
    console.log('Example app listening on port 3000');
});
